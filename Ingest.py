# Databricks notebook source
dbutils.widgets.text('filepath', '', '0 File Path')

# COMMAND ----------

import os
filepath = dbutils.widgets.get('filepath')
fname = os.path.basename(filepath)

# COMMAND ----------

import mlflow
mlflow.set_experiment('/Shared/DfO/Experiments/caserones')
mlflow.start_run(run_name=fname)
fname = fname.replace('.csv', '')
this_run_id = mlflow.active_run().info.run_id
mlflow.set_tag('Customer', 'caserones')
mlflow.set_tag('RunID', this_run_id)
mlflow.set_tag('Stage', 'postdrill')

# COMMAND ----------

# Shifts the values all columns from 'start_idx' to the left (ie: the values of column at index i will be the values of column i+1)
def shift_left(df, start_idx):
  for i in range(start_idx, len(df.columns)-1):
    df = df.withColumn(df.columns[i], col(df.columns[i+1]))
  return df

# COMMAND ----------

from pyspark.sql.types import StructType, StructField, StringType, IntegerType, LongType, DoubleType, TimestampType, BooleanType
from pyspark.sql.functions import col, lit, concat

# Only DepthFeet and ROP use , as a decimal separator
# All in metres, despite column being called "DepthFeet" and not "DepthMetres" (Stu says 99% sure)
# Stu: Duplicates in hole ids could mean redrilled holes

schema = StructType([
  StructField('Machine',StringType(),True),
  StructField('Malla',StringType(),True),
  StructField('Hole',StringType(),True),
  StructField('InicioPrevail',TimestampType(),True),
  StructField('InicioJigsaw',TimestampType(),True),
  StructField('FinPrevail',TimestampType(),True),
  StructField('FinJigsaw',TimestampType(),True),
  StructField('TiempoPerfJigsaw',DoubleType(),True),
  StructField('TiempoPerfPrevail',DoubleType(),True),
  StructField('ProfundidadJigsaw',DoubleType(),True),
  StructField('ProfundidadPrevail',DoubleType(),True),
  StructField('DiseñoJigsaw',DoubleType(),True),
  StructField('TopEste',DoubleType(),True),
  StructField('TopNorte',DoubleType(),True),
  StructField('TopElevation',DoubleType(),True),
  StructField('BottomEste',DoubleType(),True),
  StructField('BottomNorte',DoubleType(),True),
  StructField('BottomElevation',DoubleType(),True),
  StructField('PrevailHoleId',LongType(),True),
  StructField('DepthFeet',LongType(),True),
  StructField('Efforts',LongType(),True),
  StructField('Torque',StringType(),True),
  StructField('RPM',DoubleType(),True),
  StructField('ROP',DoubleType(),True),
  StructField('PullDown',LongType(),True),
  StructField('BitAir',LongType(),True),
  StructField('WeightOnBit',DoubleType(),True),
  StructField('AutoDrill',DoubleType(),True),
  StructField('_c0',DoubleType(),True),
  StructField('_c1',BooleanType(),True)
])

df = spark.read.csv(filepath, header=True, schema=schema, timestampFormat='yyyy-MM-ddTHH:mm:ss')
# fix formatting issues
df = df\
  .withColumn('DepthFeet', concat(col('DepthFeet').cast('string'), lit('.'), col('Efforts').cast('string')).cast('double') )\
  .withColumn('PullDown', concat(col('PullDown').cast('string'), lit('.'), col('BitAir').cast('string')).cast('double') )\
  .withColumn('BitAir', col('BitAir').cast('double'))\
  .withColumn('Efforts', col('Efforts').cast('double'))
df = shift_left(df, 20) # DepthFeet+1
df = shift_left(df, 24) # ROP+1
df = df.drop('_c0', '_c1')

TMP_DIR = f'/mnt/dfo/tmp/{this_run_id}/'
tmp_path = TMP_DIR+fname
df.write.mode('overwrite').format('delta').save(tmp_path)
mlflow.log_artifact('/dbfs/'+tmp_path)
dbutils.fs.rm(TMP_DIR, recurse=True)

# COMMAND ----------

mlflow.end_run()
dbutils.notebook.exit(f'{this_run_id},{fname}')

# COMMAND ----------


