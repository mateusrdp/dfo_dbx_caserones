# Databricks notebook source
# MAGIC %run "../dbx_utils/Cleaning Utils"

# COMMAND ----------

# MAGIC %run "../dbx_utils/Cluster Utils"

# COMMAND ----------

# MAGIC %run "../dbx_utils/Wrangling Utils"

# COMMAND ----------

import mlflow

# COMMAND ----------

dbutils.widgets.text('run_id', '', '0 Run ID')
dbutils.widgets.text('filename', '', '1 File Name')

# COMMAND ----------

run_id = dbutils.widgets.get('run_id')
fname = dbutils.widgets.get('filename')

from mlflow.tracking import MlflowClient
this_run = MlflowClient().get_run(run_id)
df_mwd = spark.read.format('delta').load(f'{this_run.info.artifact_uri}/{fname}').repartition(getNumCores())

# COMMAND ----------

# mlflow.set_experiment('/Shared/DfO/004 Ingest New Data/caserones/predictions')
mlflow.start_run(run_id=run_id) 
mlflow.set_tag('Customer', 'caserones')

# COMMAND ----------

display(df_mwd)

# COMMAND ----------

# Redo all hole names because some are missing.
from pyspark.sql import Window
from pyspark.sql.functions import row_number
df_holes = df_mwd.select('Malla', 'TopEste', 'TopNorte').distinct()
df_holes = df_holes.withColumn('Hole', row_number().over(Window.partitionBy('Malla').orderBy('TopEste', 'TopNorte')).cast('string'))
df_mwd = df_mwd.drop('Hole')
df_mwd = df_mwd.join(df_holes, ['Malla', 'TopEste', 'TopNorte'])

# COMMAND ----------

# From Stu: they use different bit diameters, and the choice is complicated and not recorded anywhere. He said it was agreed with site that it was reasonable
# to assume that all production holes (identified by Malla being of the form of xxPxxxx_xxx) have 311mm bits and that should be enough for us.
df_mwd = df_mwd.where(col('Malla').rlike('\w\wP.*'))

# COMMAND ----------

if df_mwd.rdd.isEmpty():
  mlflow.end_run()
  dbutils.notebook.exit(f'ERROR: there are no production holes in {fname}! Cannot infer BitDiameter!')

# COMMAND ----------

from pyspark.sql.functions import col, lit#, log as spark_log

df_mwd_clean = df_mwd\
  .where('TopEste!=BottomEste and TopNorte!=BottomNorte')\
  .where(col('TopElevation').between(4000,4550))\
  .where(col('DepthFeet').between(0,20))\
  .where(col('Torque').between(0,13611))\
  .where(col('RPM').between(0,90))\
  .where(col('ROP').between(0,10))\
  .where(col('WeightOnBit').between(0,90))\
  .withColumn('BitDiameter', lit(311))

# COMMAND ----------

TMP_DIR = f'/mnt/dfo/tmp/{run_id}/'
clean_fname = 'clean_'+fname
tmp_path = TMP_DIR+clean_fname
df_mwd_clean.write.mode('overwrite').format('delta').save(tmp_path)
mlflow.log_artifact('/dbfs/'+tmp_path)
dbutils.fs.rm(TMP_DIR, recurse=True)

# COMMAND ----------

mlflow.end_run()
dbutils.notebook.exit(clean_fname)

# COMMAND ----------

display(df_mwd_clean)

# COMMAND ----------


