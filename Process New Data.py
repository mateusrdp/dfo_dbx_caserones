# Databricks notebook source
# MAGIC %md
# MAGIC # New Data Ingestion
# MAGIC 
# MAGIC This notebook will be called when a new file is created in the customer's Storage Account. 
# MAGIC The purpose of this notebook is to:
# MAGIC 1. Identify what kind of data was uploaded
# MAGIC 1. Do all checks and balances on the new data
# MAGIC   1. Is the data complete (as in, enough information to wrangle into the appropriate DfO format)
# MAGIC   1. Is the data correct (as in, enough information to make predictions with)
# MAGIC 1. If the data is appropriate:
# MAGIC   1. Wrangle all data in the expected DfO format
# MAGIC   1. Run the appropriate prediction notebook with it
# MAGIC   
# MAGIC We need to do this on a per-customer base because:
# MAGIC 1. Each customer will have a different system, which may need to have features engineered (eg: MSE calculated)
# MAGIC   1. Can't expect the customer to engineer features because we'd like to
# MAGIC     1. Protect our IP
# MAGIC     1. Ascertain the correctness of the values (the raw, original values, may need to be processed beforehand) for reliability and to enable debugging
# MAGIC 1. Each customer may have different needs (eg: we'd like to have split the ingestion code into Pre and Post drill data for some, for example, while others may need only MWD)

# COMMAND ----------

dbutils.widgets.text('filepath', '', 'New File Path')

# COMMAND ----------

import os
fpath = dbutils.widgets.get('filepath')
fname = os.path.basename(fpath)
if fname.upper().startswith('MWD') and fname.lower().endswith('.csv'):
  ingestion_type = 'MWD'
else:
  ingestion_type = None

# COMMAND ----------

# MAGIC %md
# MAGIC For caserones we expect this file to be a like that mtmprototype2.csv file they had.
# MAGIC Assumptions:
# MAGIC 1. It'll be named as: MWD_yyyyMMddHHmmss.csv where yyyyMMddHHmmss is the full timestamp of when the file was generated down to the second.
# MAGIC 1. It'll follow the schema proposed below 
# MAGIC   1. At the moment, I'm assuming the , instead of . for decimals issue IS NOT corrected yet.
# MAGIC 1. We'll only consider Production holes
# MAGIC   1. The production holes can be identified by it's "Malla": it'll look like "xxP..." (two chars, then the capital P, then more stuff)
# MAGIC 1. The bit diameter (for these production holes) is always 311mm
# MAGIC 1. The timestamp format used is 'yyyy-MM-ddTHH:mm:ss'
# MAGIC 
# MAGIC **TODO:** remove the column merging and shifting when they finally fix this.

# COMMAND ----------

if ingestion_type == 'MWD':
  timeout = 1*24*60*60 # 1 day timeout
  print(f'-> Ingesting {fpath}')
  run_id, orig_fname = dbutils.notebook.run('Ingest', timeout, {'run_name':'Test', 'filepath':fpath}).split(sep=',')
  if 'ERROR' in orig_fname:
    dbutils.notebook.exit(orig_fname)
  print(f'-> Cleaning {orig_fname}')
  clean_fname = dbutils.notebook.run('Clean', timeout, {'run_id':run_id, 'filename':orig_fname})
  if 'ERROR' in clean_fname:
    dbutils.notebook.exit(clean_fname)
  print(f'-> Wrangling {clean_fname}')
  wrangled_fname = dbutils.notebook.run('Wrangle', timeout, {'run_id':run_id, 'filename':clean_fname})
  if 'ERROR' in wrangled_fname:
    dbutils.notebook.exit(wrangled_fname)
  print(f'-> Predicting classes for {wrangled_fname}')
  predictions_fname = dbutils.notebook.run('../dfo_dbx/Predict/Predict', timeout, {'run_id':run_id, 'filename':wrangled_fname})
  if 'ERROR' in predictions_fname:
    dbutils.notebook.exit(predictions_fname)
  print(f'-> Adding predictions in {wrangled_fname} to the full customer dataset')
  dbutils.notebook.run('../dfo_dbx/Collate Data/New Predictions Update', timeout, {'run_id':run_id, 'filename':predictions_fname})
  dbutils.notebook.exit(predictions_fname)

# COMMAND ----------

dbutils.notebook.entry_point.getDbutils().notebook().getContext().notebookPath().get()
