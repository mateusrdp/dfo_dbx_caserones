# Databricks notebook source
# MAGIC %fs
# MAGIC ls /mnt/caserones-archive/20210908

# COMMAND ----------

dbutils.widgets.text('filepath', '', 'New File Path')

# COMMAND ----------

import os
fpath = dbutils.widgets.get('filepath')
fname = os.path.basename(fpath)

# COMMAND ----------

# MAGIC %md
# MAGIC For caserones we expect this file to be a like that mtmprototype2.csv file they had.
# MAGIC Assumptions:
# MAGIC 1. It'll be named as: MWD_yyyyMMddHHmmss.csv where yyyyMMddHHmmss is the full timestamp of when the file was generated down to the second.
# MAGIC 1. It'll follow the schema proposed below 
# MAGIC   1. At the moment, I'm assuming the , instead of . for decimals issue IS NOT corrected yet.
# MAGIC 1. We'll only consider Production holes
# MAGIC   1. The production holes can be identified by it's "Malla": it'll look like "xxP..." (two chars, then the capital P, then more stuff)
# MAGIC 1. The bit diameter (for these production holes) is always 311mm
# MAGIC 1. The timestamp format used is 'yyyy-MM-ddTHH:mm:ss'
# MAGIC 
# MAGIC **TODO:** remove the column merging and shifting when they finally fix this.

# COMMAND ----------

timeout = 1*24*60*60 # 1 day timeout
print(f'-> Ingesting {fpath}')
run_id, orig_fname = dbutils.notebook.run('Ingest', timeout, {'filepath':fpath}).split(sep=',')
if 'ERROR' in orig_fname:
    dbutils.notebook.exit(orig_fname)
print(f'\tRun ID: {run_id}')
print(f'->Cleaning {orig_fname}')
clean_fname = dbutils.notebook.run('Clean', timeout, {'run_id':run_id,'filename':orig_fname})
if 'ERROR' in clean_fname:
    dbutils.notebook.exit(clean_fname)
print(f'->Wrangling {clean_fname}')
wrangled_fname = dbutils.notebook.run('Wrangle', timeout, {'run_id':run_id,'filename':clean_fname})
if 'ERROR' in wrangled_fname:
    dbutils.notebook.exit(wrangled_fname)
if 'ERROR' in wrangled_fname:
    dbutils.notebook.exit(wrangled_fname)
print(f'->Labelling {wrangled_fname}')
labelling_args = {
  'run_id':run_id, 
  'filename':wrangled_fname,
  'K':'4',
  'random_seed':'42',
  'kmeans_restarts':'10',
  'kmeans_maxiter':'20',
  'precond_len':'3.0',
  'feat_col':'MachineZScorelogMSE'
}
labelled_fname = dbutils.notebook.run('../dfo_dbx/Label Samples/2D MWD to Hardness', timeout, labelling_args)
if 'ERROR' in labelled_fname:
    dbutils.notebook.exit(labelled_fname)
print(f'->Training a model with {labelled_fname}')
training_args = {
  'run_id':run_id, 
  'filename':labelled_fname,
  'filename_digdata':'',
  'mwd_feat':'MachineZScorelogMSE',
  'geo_feats':'',
  'precond_len':'0.0',
  'use_ccl':'yes',
  'blast_performance_col':'',
  'winRad':'1.0',
  'minIDRPct':'70.0',
  'use_fs':'no',
  'confusion_histogram':'yes'
}
dbutils.notebook.run('../dfo_dbx/Train Models/2D Anything to Hardness', timeout, training_args)
print(f'->Replacing full set of blasts with {labelled_fname}')
dbutils.notebook.run('../dfo_dbx/Collate Data/New Historical', timeout, {'run_id':run_id,'filename':labelled_fname})

# COMMAND ----------


