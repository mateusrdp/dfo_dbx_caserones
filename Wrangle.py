# Databricks notebook source
# MAGIC %run "../dbx_utils/Cleaning Utils"

# COMMAND ----------

# MAGIC %run "../dbx_utils/Cluster Utils"

# COMMAND ----------

# MAGIC %run "../dfo_dbx/DfO Utils/DfO MLlib"

# COMMAND ----------

# MAGIC %run "../dbx_utils/Wrangling Utils"

# COMMAND ----------

import mlflow

# COMMAND ----------

dbutils.widgets.text('run_id', '', '0 Run ID')
dbutils.widgets.text('filename', '', '1 File Name')

# COMMAND ----------

run_id = dbutils.widgets.get('run_id')
fname = dbutils.widgets.get('filename')

# COMMAND ----------

mlflow.start_run(run_id=run_id)
mlflow.set_tag('Customer', 'caserones')

# COMMAND ----------

from mlflow.tracking import MlflowClient
this_run = MlflowClient().get_run(run_id)
df_mwd = spark.read.format('delta').load(f'{this_run.info.artifact_uri}/{fname}').repartition(getNumCores())

# COMMAND ----------

coord_adder = AddDownholeCoords(
  collarCoordinateCols=['TopEste', 'TopNorte', 'TopElevation'], toeCoordinateCols=['BottomEste', 'BottomNorte', 'BottomElevation'], holeDepthCol='ProfundidadJigsaw', 
  zenithCol=None, azimuthCol=None, inRadians=False, isComplZenith=False, 
  sampleDepthCol='DepthFeet', outputCols=['SampleEasting', 'SampleNorthing', 'SampleElevation']
)
df_mwd_wrangled = coord_adder.transform(df_mwd)

# COMMAND ----------

from pyspark.sql.functions import lit, log as spark_log
start_time = time()
df_mwd_wrangled = df_mwd_wrangled.withColumn('MSE', calculateMSE(col('WeightOnBit'), col('RPM'), col('Torque'), col('ROP'), col('BitDiameter')))
df_mwd_wrangled = df_mwd_wrangled.withColumn('logMSE', spark_log('mse'))
df_mwd_wrangled = standardise(df_mwd_wrangled, 'Machine', 'MSE')
df_mwd_wrangled = standardise(df_mwd_wrangled, 'Machine', 'logMSE')

# COMMAND ----------

from pyspark.sql.functions import concat
df_mwd_wrangled = df_mwd_wrangled.withColumn('HoleNumber', concat(col('Malla'), lit('_'), col('Hole') ))

# COMMAND ----------

df_BlastHole = df_mwd_wrangled\
  .select(
    col('FinPrevail').alias('TimeStamp'),
    col('Malla').alias('DrillPattern'),
    'HoleNumber',
    col('Machine').alias('DrillNumber'),
    col('ProfundidadJigsaw').alias('HoleDepth'),
    col('TopEste').alias('CollarEasting'),
    col('TopNorte').alias('CollarNorthing'),
    col('TopElevation').alias('CollarElevation'),
    col('DepthFeet').alias('SampleDepth'), # Stu says it's in metres regardless of the name
    'SampleEasting', 
    'SampleNorthing',
    'SampleElevation',
    'MSE', # using MSE calculated using Teale's formula
    'logMSE',
    'MachineZScoreMSE',
    'MachineZScorelogMSE'
  )

# COMMAND ----------

TMP_DIR = f'/mnt/dfo/tmp/{run_id}/'
wrangled_fname = 'wrangled_'+fname
tmp_path = TMP_DIR+wrangled_fname
df_BlastHole.write.mode('overwrite').format('delta').save(tmp_path)
mlflow.log_artifact('/dbfs/'+tmp_path)
dbutils.fs.rm(TMP_DIR, recurse=True)

# COMMAND ----------

mlflow.end_run()
dbutils.notebook.exit(wrangled_fname)

# COMMAND ----------

display(df_BlastHole)

# COMMAND ----------


